﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Collections;
using System.Threading;

namespace Server_Eco_Chat
{
    public class PegaCliente
    {
        //Declarando variaveis
        TcpClient clienteSocket;
        string nomeCliente;
        Hashtable listaClientes;

        public void startClient(TcpClient inClientSocket, string clineNo, Hashtable cList)
        {
            this.clienteSocket = inClientSocket;
            this.nomeCliente = clineNo;
            this.listaClientes = cList;
            //Iniciando thread que cuida dos envios das mensagens
            Thread ctThread = new Thread(Chat);
            ctThread.Start();
        }

        private void Chat()
        {

            try
            {
                while (true)
                {
                    //Trata a mensagem do cliente para ser enviada
                    byte[] bytesFrom = new byte[1024];
                    string msgCliente = null;
                    NetworkStream redeStream = clienteSocket.GetStream();
                    redeStream.Read(bytesFrom, 0, 1024);
                    msgCliente = Encoding.UTF8.GetString(bytesFrom);
                    msgCliente = msgCliente.Substring(0, msgCliente.IndexOf("$"));
                    Console.WriteLine(nomeCliente + " (" + DateTime.Now.ToString("HH:mm") + "): \r\n " + msgCliente);

                    //Declarando mensagens a serem enviadas para todos outros clientes
                    Program.transmissor(msgCliente, nomeCliente, true);
                 }

             }
             catch (Exception)
             {
                Console.WriteLine(nomeCliente + " Saiu do chat");
                listaClientes.Remove(nomeCliente);
            }
        }//fim doChat
    } //fim classe PegaCliente
}
