﻿using System;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Collections;

namespace Server_Eco_Chat
{
    class Program
    {
        //Iniciando classe hashtable para lista de clientes
        public static Hashtable listaClientes = new Hashtable();

        static void Main(string[] args)
        {
            try
            {
                //Declarando variavel de porta
                const int PORT = 666;

                //Convertendo Ip de string para IPAddress
                IPAddress IP_SERVER = IPAddress.Parse(IpLocal());

                //Declara as variaveis de ip e porta na classe listener
                TcpListener serverSocket = new TcpListener(IP_SERVER, PORT);

                //Inicia atendimento aos pedidos de clientes
                serverSocket.Start();

                Console.WriteLine("Servidor iniciado com sucesso.\r\nDados para conexão --> IP: " + IP_SERVER.ToString() + "  --> Porta: " + PORT);

                //loop para classe listener
                while (true)
                {
                    //Aceita os pedidos de conexao
                    TcpClient clienteSocket = serverSocket.AcceptTcpClient();

                    Console.WriteLine("Conectado com sucessso!");

                    //Pegando objetos para envio e recebimento
                    NetworkStream networkStream = clienteSocket.GetStream();
                    
                    //Tratamento das informações digitadas a serem enviadas
                    byte[] bytesFrom = new byte[(int)clienteSocket.ReceiveBufferSize];
                    string nomeCliente = null;
                    networkStream.Read(bytesFrom, 0, (int)clienteSocket.ReceiveBufferSize);
                    nomeCliente = Encoding.UTF8.GetString(bytesFrom);
                    nomeCliente = nomeCliente.Substring(0, nomeCliente.IndexOf("$"));

                    //Adiciona cliente na lista hashtable
                    try
                    {
                        listaClientes.Add(nomeCliente, clienteSocket);
                    }
                    catch
                    {
                        
                    }
                    //Avisa aos clientes quem entrou no chat
                    transmissor(nomeCliente + " Entrou ", nomeCliente, false);

                    //Mensagem no servidor de quem entrou no chat
                    Console.WriteLine(nomeCliente + " Entrou no chat ");

                    //Inicia classe que trata  as mensagens dos clientes a serem enviadas
                    PegaCliente client = new PegaCliente();
                    client.startClient(clienteSocket, nomeCliente, listaClientes);
                }
            }
            catch (SocketException e)
            {
                Console.WriteLine("SocketException: {0}", e);
            }
        }
        //Método para detectar IP da maquina
        public static string IpLocal()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            throw new Exception("IP local nao encontrato");
        }

        //Funcão que trasmite a mensagem recebida de um cliente para todos do chat
        public static void transmissor(string msg, string uName, bool flag)
        {
            //Para cada cliente na lista cliente...
            foreach (DictionaryEntry Item in listaClientes)
            {
                try
                {
                    TcpClient transmissorSocket;
                    transmissorSocket = (TcpClient)Item.Value;
                    NetworkStream transmissorStream = transmissorSocket.GetStream();
                    Byte[] transmissorBytes = null;

                    if (flag == true)
                    {
                        transmissorBytes = Encoding.UTF8.GetBytes(uName + " (" + DateTime.Now.ToString("HH: mm") + "): \r\n " + msg);
                    }
                    else
                    {
                        transmissorBytes = Encoding.UTF8.GetBytes(msg);
                    }

                    transmissorStream.Write(transmissorBytes, 0, transmissorBytes.Length);
                    transmissorStream.Flush();
                }
                catch
                {
                }
            }//fim foreach
        }//fim funcao transmissor
    }//fim Main class
}//fim namespace